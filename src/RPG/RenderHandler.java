package RPG;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

class RenderHandler {
    private BufferedImage view;
    private Rectangle camera;
    private int[] pixels;

    RenderHandler(int width, int height) {
        //GraphicsDevice[] graphicsDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

        //Create a BufferedImage that will represent our view.
        view = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        //Create an array for pixels
        pixels = ((DataBufferInt) view.getRaster().getDataBuffer()).getData();

        camera = new Rectangle(0, 0, width, height);
    }

    void render(Graphics graphics) {
        //draw preRendered image to graphics
        graphics.drawImage(view, 0, 0, view.getWidth(), view.getHeight(), null);
    }

    void renderSprite(Sprite sprite, int xPosition, int yPosition, int xZoom, int yZoom, boolean fixed) {
        renderArray(sprite.getPixels(), sprite.getWidth(), sprite.getHeight(), xPosition, yPosition, xZoom, yZoom, fixed);
    }

    /**
     * Render this image to whole image with x, y position
     * (Ex: for drawing a tiles)
     */
    void renderImage(BufferedImage image, int xPosition, int yPosition, int xZoom, int yZoom, boolean fixed) {
        int imagePixels[] = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        renderArray(imagePixels, image.getWidth(), image.getHeight(), xPosition, yPosition, xZoom, yZoom, fixed);
    }

    private void renderArray(int[] renderPixels, int renderWidth, int renderHeight,
                             int xPosition, int yPosition, int xZoom, int yZoom, boolean fixed) {
        for (int y = 0; y < renderHeight; y++) {
            for (int x = 0; x < renderWidth; x++) {
                for (int yZoomPosition = 0; yZoomPosition < yZoom; yZoomPosition++) {
                    for (int xZoomPosition = 0; xZoomPosition < xZoom; xZoomPosition++) {
                        setPixel(renderPixels[x + y * renderWidth],
                                (x * xZoom) + xPosition + xZoomPosition,
                                ((y * yZoom) + yPosition + yZoomPosition),
                                fixed);
                    }
                }
            }
        }
    }

    void renderRectangle(Rectangle rectangle, int xZoom, int yZoom, boolean fixed) {
        int[] rectanglePixels = rectangle.getPixels();
        if (rectanglePixels != null)
            renderArray(rectanglePixels, rectangle.width, rectangle.height, rectangle.x, rectangle.y, xZoom, yZoom, fixed);
    }

    void renderRectangle(Rectangle rectangle, Rectangle offset, int xZoom, int yZoom, boolean fixed) {
        int[] rectanglePixels = rectangle.getPixels();
        if (rectanglePixels != null)
            renderArray(rectanglePixels, rectangle.width, rectangle.height, rectangle.x + offset.x,
                    rectangle.y + offset.y, xZoom, yZoom, fixed);
    }

    //Checks if the zooming pictures is fitting in this frame(screen) to avoid IndexOutOfBoundException
    private void setPixel(int pixel, int x, int y, boolean fixed) {
        int pixelIndex = 0;

        if (!fixed) {
            if (x >= camera.x && y >= camera.y && x <= camera.x + camera.width && y <= camera.y + camera.height) {
                pixelIndex = (x - camera.x) + (y - camera.y) * view.getWidth();
            }
        } else {
            if (x >= 0 && y >= 0 && x <= camera.width && y <= camera.height) {
                pixelIndex = x + y * view.getWidth();
            }
        }

        if (pixels.length > pixelIndex && pixel != Game.alpha)
            pixels[pixelIndex] = pixel;
    }

    void renderPlayerCoordinates(Graphics graphics, Player player, int xZoom, int yZoom) {
        int x = player.getPlayerX(xZoom);
        int y = player.getPlayerY(yZoom);

        graphics.setFont(new Font("Dialog", Font.PLAIN, 18));
        graphics.drawString("XY: " + x + "," + y, 10, 15);
    }

    void renderFPS(Graphics graphics, int fps, int averageFps) {
        graphics.setColor(Color.YELLOW);
        graphics.setFont(new Font("Dialog", Font.BOLD, 18));
        graphics.drawString("FPS: " + fps, 10, 35);
        graphics.drawString("avFPS: " + averageFps, 10, 55);
    }

    Rectangle getCamera() {
        return camera;
    }

    void clear() {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = 0;
        }
    }
}
