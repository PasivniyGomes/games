package RPG;

public class Player implements GameObject {

    private Rectangle playerRectangle;
    private Sprite sprite;
    private AnimatedSprite animatedSprite = null;
    private int speed = 10;
    private int direction = 3;  //0 - RIGHT, 1 - LEFT, 2 - UP, 3 - DOWN


    public Player(Sprite sprite) {
        if (sprite instanceof AnimatedSprite) {
            animatedSprite = (AnimatedSprite) sprite;
        }

        updateDirection();
        playerRectangle = new Rectangle(-5, 0, 16, 16);
        playerRectangle.generateGraphics(3, 0xFF00FF90);
    }

    int getPlayerX(int xZoom) {
        double x = ((playerRectangle.x - playerRectangle.width) / 16.0) / xZoom;
        if (x <= 0) {
            return (int) x;
        }
        if (x % 1 != 1) {
            return (int) x + 1;
        }
        return (int) x;
    }

    int getPlayerY(int yZoom) {
        double y = ((playerRectangle.y - playerRectangle.height) / 16.0) / yZoom;
        if (y <= 0) {
            return (int) y;
        }
        if (y % 1 != 1) {
            return (int) y + 1;
        }
        return (int) y;
    }

    @Override
    public boolean handleMouseClick(Rectangle mouseRectangle, Rectangle camera, int xZoom, int yZoom) {
        return false;
    }

    @Override
    public void render(RenderHandler renderer, int xZoom, int yZoom) {
        if (animatedSprite != null) {
            renderer.renderSprite(animatedSprite, playerRectangle.x, playerRectangle.y, xZoom, yZoom, false);
        } else if (sprite != null) {
            renderer.renderSprite(sprite, playerRectangle.x, playerRectangle.y, xZoom, yZoom, false);
        } else {
            renderer.renderRectangle(playerRectangle, xZoom, yZoom, false);
        }
    }

    @Override
    public void update(Game game) {
        KeyBoardListener keyListener = game.getKeyListener();
        boolean didMove = false;
        int newDirection = direction;

        if (keyListener.left()) {
            newDirection = 1;
            playerRectangle.x -= speed;
            didMove = true;
        }
        if (keyListener.right()) {
            newDirection = 0;
            playerRectangle.x += speed;
            didMove = true;
        }
        if (keyListener.up()) {
            newDirection = 2;
            playerRectangle.y -= speed;
            didMove = true;
        }
        if (keyListener.down()) {
            newDirection = 3;
            playerRectangle.y += speed;
            didMove = true;
        }

        if (!didMove) {
            animatedSprite.reset();
        }

        if (newDirection != direction) {
            direction = newDirection;
            updateDirection();
        }
        updateCamera(game.getRenderer().getCamera());
        if (didMove) {
            animatedSprite.update(game);
        }
    }

    private void updateDirection() {
        if (animatedSprite != null) {
            animatedSprite.setAnimationRange(direction * 8, (direction * 8) + 7);
        }
    }

    private void updateCamera(Rectangle camera) {
        camera.x = playerRectangle.x - (camera.width / 2);
        camera.y = playerRectangle.y - (camera.height / 2);
    }
}
