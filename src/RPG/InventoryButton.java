package RPG;

class InventoryButton extends GUIButton {

    private Game game;
    private boolean isSelected = false;
    private int tileID;

    InventoryButton(Game game, int tileID, Sprite tileSprite, Rectangle rect) {
        super(tileSprite, rect, true);
        this.game = game;
        this.tileID = tileID;
        rect.generateGraphics(0xFFDB3D);
    }

    @Override
    public void update(Game game) {
        if (tileID == game.getSelectedTile()) {
            if (!isSelected) {
                rect.generateGraphics(0x67FF3D);
                isSelected = true;
            }
        } else {
            if (isSelected) {
                isSelected = false;
                rect.generateGraphics(0xFFDB3D);
            }
        }
    }

    @Override
    void render(RenderHandler renderer, int xZoom, int yZoom, Rectangle interfaceRect) {
        renderer.renderRectangle(rect, interfaceRect, 1, 1, fixed);
        renderer.renderSprite(sprite,
                interfaceRect.x + rect.x + (xZoom - (xZoom - 1)) * rect.width / 2 / xZoom,
                interfaceRect.y + rect.y + (yZoom - (yZoom - 1)) * rect.height / 2 / yZoom,
                xZoom - 1,
                yZoom - 1,
                fixed);
    }

    @Override
    void activate() {
        game.changeTile(tileID);
    }
}
