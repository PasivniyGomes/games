package PointDestructionGame;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Listeners implements KeyListener, MouseListener, MouseMotionListener
{
	public void keyPressed(KeyEvent e) 
	{
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_W)
		{
			Player.up=true;
		}
		if(key == KeyEvent.VK_S)
		{
			Player.down=true;
		}
		if(key == KeyEvent.VK_A)
		{
			Player.left=true;
		}
		if(key == KeyEvent.VK_D)
		{
			Player.right=true;
		}
		if(key == KeyEvent.VK_SPACE)
		{
			Player.isFire=true;
		}
		if(key == KeyEvent.VK_ESCAPE)
		{
			GamePanel.states = GamePanel.STATE.MENUE;
		}
	}

	public void keyReleased(KeyEvent e) 
	{
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_W)
		{
			Player.up=false;
		}
		if(key == KeyEvent.VK_S)
		{
			Player.down=false;
		}
		if(key == KeyEvent.VK_A)
		{
			Player.left=false;
		}
		if(key == KeyEvent.VK_D)
		{
			Player.right=false;
		}
		if(key == KeyEvent.VK_SPACE)
		{
			Player.isFire=false;
		}
	}

	public void keyTyped(KeyEvent e) 
	{
		
	}

	public void mouseClicked(MouseEvent e) 
	{
		
	}

	public void mouseEntered(MouseEvent e) 
	{
		
	}

	public void mouseExited(MouseEvent e) 
	{
		
	}

	public void mousePressed(MouseEvent e) 
	{
		if (e.getButton() == MouseEvent.BUTTON1)
		{
			GamePanel.player.isFire= true;
			GamePanel.LeftMouse=true;
		}
	}

	public void mouseReleased(MouseEvent e) 
	{
			GamePanel.player.isFire= false;
			GamePanel.LeftMouse=false;
	}

	public void mouseDragged(MouseEvent e) 
	{
			GamePanel.mouseX=e.getX();
			GamePanel.mouseY=e.getY();
	}

	public void mouseMoved(MouseEvent e) 
	{
		if(e.getButton() == MouseEvent.MOUSE_MOVED)
		{
			GamePanel.mouseX=e.getX();
			GamePanel.mouseY=e.getY();
		}
	}
	
}
