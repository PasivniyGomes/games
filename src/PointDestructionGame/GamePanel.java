package PointDestructionGame;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable
{
	public static int WIDTH=800, HEIGHT=640;
	public static int mouseX, mouseY;
	private Thread thread = new Thread(this);
	private BufferedImage image;
	private Graphics2D graphics;
	private GameBackGround background;
	private Menue menue;
	public static Player player;
	public static ArrayList<Bullet> bullets;
	public static ArrayList<Enemy> enemies;
	public static Wave wave;
	public static boolean LeftMouse;
	
	//MENU
	public enum STATE
	{
		MENUE,
		PLAY
	}
	public static STATE states = STATE.MENUE;
	
	//Constructor
	GamePanel()
	{
		super(); //constructor JPanel(parent class)
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();//input focus
		addKeyListener(new Listeners());
		addMouseListener(new Listeners());
		addMouseMotionListener(new Listeners());
		LeftMouse=false;
	}
	
	public void startThread()
	{
		thread.start();  //call the 'run' method
	}
	
	public void run() 
	{	
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		graphics = (Graphics2D) image.getGraphics();
		background = new GameBackGround();
		menue = new Menue();
		player = new Player();
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		bullets = new ArrayList<Bullet>();
		enemies = new ArrayList<Enemy>();
		wave = new Wave();
		
		while(true)
		{
			if (states.equals(STATE.MENUE))
			{
				menue.Update();
				menue.Draw(graphics);
				GameDraw();
			}
			if(states.equals(STATE.PLAY))
			{
				gameUpdate();
				gameRender();
				GameDraw();
				try 
				{
					thread.sleep(20);  //To regulate FPS (20 nanosec. after frame generate)
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public void gameUpdate()
	{
		//UPDATE PLAYER, ENEMY ...
		background.Update();
		player.Update();
		
		//UPDATE BULLETS
		for(int i=0; i<bullets.size(); i++)
		{
			bullets.get(i).Update();
			if(bullets.get(i).Remove() == true)
			{
				bullets.remove(i);
				i--;
			}
		}
		
		//UPDATE ENEMIES
		for(int i =0; i<enemies.size(); i++)
		{
			enemies.get(i).Update();
			if(enemies.get(i).Remove() == true)
			{
				enemies.remove(i);
				i--;
			}
		}
		wave.Update();
	}
	
	public void gameRender()
	{
		//ReRENDERING FRAME
		background.Draw(graphics);
		player.Draw(graphics);
		
		//RENDER BULLETS
		for(int i=0; i<bullets.size(); i++)
		{
			bullets.get(i).Draw(graphics);
		}
		
		//BULLET_ENEMY COLLISION
		for(int i=0; i<enemies.size(); i++)
		{
			Enemy e = enemies.get(i);
			double ex = e.GetX();
			double ey = e.GetY();
			for(int j = 0; j<bullets.size(); j++)
			{
				Bullet b = bullets.get(j);
				double bx = b.GetX();
				double by = b.GetY();
				double dx = ex-bx;
				double dy = ey-by;
				double distance = Math.sqrt(dx*dx + dy*dy);
				if((int)distance < e.GetRadius()+ b.GetRadius())
				{
					enemies.get(i).OnHit();
					bullets.remove(j);
					j--;
					if(enemies.get(i).Remove() == true)
					{
						enemies.remove(i);
						i--;
					}
					break;
				}
			}
			
		}
		
		//ENEMY_PLAYER COLLISION
		for(int i=0; i<enemies.size(); i++)
		{
			Enemy e = enemies.get(i);
			double ex = e.GetX();
			double ey = e.GetY();
			double px = player.GetX();
			double py = player.GetY();
			double dx = ex-px;
			double dy = ey-py;
			double distance = Math.sqrt(dx*dx + dy*dy);
			if((int) distance < e.GetRadius() + player.GetRadius())
			{
				player.OnHit();
			}
		}
		
		//RENDER ENEMIES
		for(int i =0; i<enemies.size(); i++)
		{
			enemies.get(i).Draw(graphics);
		}
		
		//RENDER WAVE
		if(wave.ShowWave() == true)
		{
			wave.Draw(graphics);
		}
	}
	
	private void GameDraw()
	{
		Graphics visibleGraphics = this.getGraphics();
		visibleGraphics.drawImage(image, 0, 0, null);
		visibleGraphics.dispose(); //DELETE this render, after it`s drawning
	}
}
