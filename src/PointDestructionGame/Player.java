package PointDestructionGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class Player 
{
	private double x, y;
	private double dx, dy; //move coef.
	private int speed, health;
	private int playerRadius;
	private Color playerColor;
	public static boolean up, down, left, right, isFire;
	
	public Player()
	{
		x= GamePanel.WIDTH /2;
		y= GamePanel.HEIGHT/1.3;
		playerColor=Color.WHITE;
		playerRadius=5;
		up = false;
		down=false;
		left=false;
		right=false;
		isFire=false;
		speed=7;
		health=3;
		dx=0;
		dy=0;
	}
	
	public int GetHealth()
	{
		return health;
	}
	
	public double GetX()
	{
		return x;
	}
	
	public double GetY()
	{
		return y;
	}
	
	public int GetRadius()
	{
		return playerRadius;
	}
	
	public void OnHit()
	{
		health--;
	}
	
	public void Update()
	{
		if(isFire == true)
		{
			GamePanel.bullets.add(new Bullet());
		}
		if (up ==true && y>playerRadius)
		{
			dy =-speed;
		}
		if(down == true && y< GamePanel.HEIGHT-playerRadius-3)
		{
			dy =speed;
		}
		if(left == true && x >playerRadius)
		{
			dx =-speed;
		}
		if(right == true && x< GamePanel.WIDTH-playerRadius)
		{
			dx =speed;
		}
		//
		if(down && right || down && left || up && left || up && right)
		{
			double angle = Math.toRadians(45);
			dy= dy*Math.sin(angle);
			dx= dx*Math.cos(angle);
		}
		y+=dy;
		x+=dx;
		dy=0;
		dx=0;
	}
	
	public void Draw(Graphics2D graphics)
	{
		graphics.setColor(playerColor);
		graphics.fillOval((int)x-playerRadius,(int) y-playerRadius, 3*playerRadius, 3*playerRadius);
		graphics.setStroke(new BasicStroke(3));
		graphics.setColor(playerColor.darker());
		graphics.drawOval((int)x-playerRadius,(int) y-playerRadius, 3*playerRadius, 3*playerRadius);
		graphics.setStroke(new BasicStroke(1));
	}
}