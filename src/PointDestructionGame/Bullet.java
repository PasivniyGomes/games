package PointDestructionGame;

import java.awt.Color;
import java.awt.Graphics;

public class Bullet 
{
	private double x, y;
	private int r, speed;
	private Color color;
	
	public Bullet()
	{
		x=GamePanel.player.GetX();
		y=GamePanel.player.GetY();
		r=2;
		color=Color.WHITE;
		speed=10;
	}
	
	public double GetX()
	{
		return x;
	}

	public double GetY()
	{
		return y;
	}
	
	public int GetRadius()
	{
		return r;
	}
	
	public boolean Remove()
	{
		if(y<0)
		{
			return true;
		}
		return false;
	}
	
	public void Update()
	{
		y-=speed;
	}
	
	public void Draw(Graphics g)
	{
		g.setColor(color);
		g.fillOval((int) x ,(int) y, r, 2*r);
	}
}
