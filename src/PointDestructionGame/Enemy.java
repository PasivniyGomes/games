package PointDestructionGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class Enemy 
{
	private double x,y, dx, dy, angle;
	private int r, type, rank, speed, health;
	private Color color;
	
	public Enemy(int type, int rank)
	{
		this.type=type;
		this.rank=rank;
		x=Math.random()*GamePanel.WIDTH;  y=0;
		angle = Math.toRadians(Math.random()*360);
		switch (type) 
		{
		case 1:
		{
			speed=3;
			color = Color.GREEN;
			r= 7*rank;
			health=2*rank;
			break;	
		}
		case 2:
		{
			
			break;
		}
		}
		SetDXY();
	}
	
	public int GetRadius()
	{
		return r;
	}
	
	public double GetX()
	{
		return x;
	}
	
	public double GetY()
	{
		return y;
	}
	
	public void OnHit()
	{
		health--;
	}
	
	public boolean Remove()
	{
		if(health<=0)
		{
			return true;
		}
		return false;
	}
	
	public void Update()
	{
		x+=dx;
		y+=dy;
		if(x <0 && dx<0)
		{
			dx = -dx;
		}
		if (x>GamePanel.WIDTH && dx > 0)
		{
			dx=-dx;
		}
		if(y<0 && dy<0)
		{
			dy=-dy;
		}
		if(y>GamePanel.HEIGHT && dy>0)
		{
			dy= -dy;
		}
	}
	
	private void SetDXY()
	{
		dx = Math.sin(angle)*speed;
		dy =Math.cos(angle)*speed;
	}
	
	public void Draw(Graphics2D g)
	{
		g.fillOval((int)x-r,(int) y-r, 2*r, 2*r);
		g.setStroke(new BasicStroke(3));
		g.setColor(color.darker());
		g.drawOval((int)x-r,(int) y-r, 2*r, 2*r);
		g.setStroke(new BasicStroke(1));
	}
}
