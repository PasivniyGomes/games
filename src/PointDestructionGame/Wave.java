package PointDestructionGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class Wave 
{
	private int waveNumber, waveMultiplier;
	private long waveTimer, waveDelay, waveTimerDifference;
	private String waveText;
	
	public Wave()
	{
		waveNumber=1;
		waveTimer=0;
		waveDelay=5000; //5 sec
		waveTimerDifference = 0;
		waveText="Wave "+waveNumber;
		waveMultiplier=5;
	}
	
	public void CreateEnemies()
	{
		int enemyCount=waveNumber*waveMultiplier;
		if(waveNumber<4)
		{
			while(enemyCount > 0)
			{
				int type=1, rank=1;
				GamePanel.enemies.add(new Enemy(type, rank));
				enemyCount-=type*rank;
			}
		}
	}
	
	public void Update()
	{
		if(GamePanel.enemies.size() == 0 && waveTimer == 0)
		{
			waveTimer=System.nanoTime();
			waveText="Wave "+waveNumber;
			waveNumber++;
		}
		if(waveTimer > 0)
		{
			waveTimerDifference += (System.nanoTime()-waveTimer)/1000000;
			waveTimer = System.nanoTime();
		}
		if(waveTimerDifference > waveDelay)
		{
			CreateEnemies();
			waveTimer =0;
			waveTimerDifference=0;
		}
	}
	
	public boolean ShowWave()
	{
		if(waveTimer !=0)
		{
			return true;
		}
		else return false;
	}
	
	public void Draw(Graphics2D g)
	{
		double divider = waveDelay/180;
		double alpha = waveTimerDifference/divider;
		alpha= 255*Math.sin(Math.toRadians(alpha));
		if(alpha<=0)
		{
			alpha=0;
		}
		if(alpha>=255)
		{
			alpha=255;
		}
		g.setFont(new Font("consolas", Font.PLAIN, 20));
		long length = (long) g.getFontMetrics().getStringBounds(waveText, g).getWidth();
		g.setColor(new Color(255,255,255,(int) alpha));
		g.drawString(waveText, GamePanel.WIDTH/2 - (int) length/2, GamePanel.HEIGHT/2);
	}
	
}
