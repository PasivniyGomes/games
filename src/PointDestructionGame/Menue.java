package PointDestructionGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class Menue 
{
	private Color backgroundColor;
	private Color buttonColor, textColor;
	private int buttonWidth, buttonHeight;
	private String menueText;
	public Menue() 
	{
		backgroundColor = Color.GRAY;
		buttonColor = Color.MAGENTA;
		buttonHeight=50;
		buttonWidth=160;
		menueText="PAY FOR GAME";
		textColor = Color.BLACK;
	}
	
	public void Update()
	{
		if(GamePanel.mouseX > GamePanel.WIDTH/2-buttonWidth/2 && GamePanel.mouseX< GamePanel.WIDTH/2+buttonWidth/2 
			&& GamePanel.mouseY > GamePanel.HEIGHT/2-buttonHeight/2 && GamePanel.mouseY < GamePanel.HEIGHT/2+buttonHeight/2)
		{
			if(GamePanel.LeftMouse == true)
			{
				GamePanel.states = GamePanel.STATE.PLAY;
			}
		}
	}
	
	public void Draw(Graphics2D graphics)
	{
		graphics.setColor(backgroundColor);
		graphics.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		
		graphics.setColor(buttonColor);
		graphics.fillRect(GamePanel.WIDTH/2 - buttonWidth/2, GamePanel.HEIGHT/2-buttonHeight/2, buttonWidth, buttonHeight);
		graphics.setColor(buttonColor.darker());
		graphics.setStroke(new BasicStroke(5));
		graphics.drawRect(GamePanel.WIDTH/2 - buttonWidth/2, GamePanel.HEIGHT/2-buttonHeight/2, buttonWidth, buttonHeight);
		graphics.setStroke(new BasicStroke(1));
		
		graphics.setColor(textColor);
		graphics.setFont(new Font("consolas", Font.BOLD, 20));
		graphics.drawString(menueText, GamePanel.WIDTH/2 - buttonWidth/2+19, GamePanel.HEIGHT/2-buttonHeight/2+30);
	}
}
