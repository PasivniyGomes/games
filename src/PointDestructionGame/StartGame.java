package PointDestructionGame;
import javax.swing.JFrame;

public class StartGame 
{
	public static void game()
	{
		GamePanel gamepanel = new GamePanel();
		
		JFrame startFrame = new JFrame("BubleShooter");
		startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		startFrame.setContentPane(gamepanel);
		startFrame.pack();  //Causes this Window to be sized to fit the preferred sizeand layouts of its subcomponents
		startFrame.setVisible(true);
		
		gamepanel.startThread();
	}
}
