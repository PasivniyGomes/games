package PointDestructionGame;
import java.awt.Color;
import java.awt.Graphics2D;

public class GameBackGround 
{
	private Color color;
	
	public GameBackGround() 
	{
		color = Color.BLUE;
	}
	
	public void Update()
	{
		
	}
	
	public void Draw(Graphics2D graphics)
	{
		graphics.setColor(color);
		graphics.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
	}
}
