package ConwaysGameOfLife;

import java.awt.*;

class Cell {

    private int x;
    private int y;
    private State state;
    private State nextState;
    private State customCreateState;
    static int LENGTH = 20;

    public enum State {
        DEAD, ALIVE
    }

    Cell(int x, int y, State state) {
        this.x = x;
        this.y = y;
        this.state = state;
        nextState = state;
        customCreateState = state;
    }

    void draw(Graphics graphics) {
        Graphics2D g2 = (Graphics2D) graphics;
        g2.setStroke(new BasicStroke(1));

        if (state == State.DEAD) {
            g2.setColor(Color.WHITE);
            g2.fillRect(x, y, LENGTH, LENGTH);
            g2.setColor(Color.BLACK);
            g2.drawRect(x, y, LENGTH, LENGTH);
        } else if (state == State.ALIVE) {
            g2.setColor(Color.BLACK);
            g2.fillRect(x, y, LENGTH, LENGTH);
            g2.setColor(Color.GRAY);
            g2.drawRect(x, y, LENGTH, LENGTH);
        }
        state = nextState;
    }

    void drawCustomMode(Graphics graphics) {
        Graphics2D g2 = (Graphics2D) graphics;
        g2.setStroke(new BasicStroke(1));

        if (customCreateState == State.DEAD) {
            g2.setColor(Color.WHITE);
            g2.fillRect(x, y, LENGTH, LENGTH);
            g2.setColor(Color.BLACK);
            g2.drawRect(x, y, LENGTH, LENGTH);
        } else if (customCreateState == State.ALIVE) {
            g2.setColor(Color.BLACK);
            g2.fillRect(x, y, LENGTH, LENGTH);
            g2.setColor(Color.GRAY);
            g2.drawRect(x, y, LENGTH, LENGTH);
        }
    }

    void nextGeneration(int aliveNeighbours) {
        if (state == State.ALIVE) {
            if (aliveNeighbours < 2) {
                nextState = State.DEAD;
            } else if (aliveNeighbours <= 3) {
                nextState = State.ALIVE;
            } else {
                nextState = State.DEAD;
            }
        }
        if (state == State.DEAD) {
            if (aliveNeighbours == 3) {
                nextState = State.ALIVE;
            }
        }
    }

    State getState() {
        return state;
    }

    void revertState() {
        if (state == State.ALIVE) {
            nextState = State.DEAD;
            if (Main.onBoardCustomCreate) {
                state = State.DEAD;
                customCreateState = State.DEAD;
            }
        } else {
            nextState = State.ALIVE;
            if (Main.onBoardCustomCreate) {
                customCreateState = State.ALIVE;
            }
        }
    }
}
