package ConwaysGameOfLife;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

class Board {
    private List<List<Cell>> cells = new ArrayList<>();
    private List<List<Cell.State>> currentCellsStates = new ArrayList<>();
    static int X;
    static int Y;

    void createBoard() {
        for (int i = 0; i < Main.WIDTH / Cell.LENGTH; i++) {
            cells.add(i, new ArrayList<>());
            for (int j = 0; j < Main.HEIGHT / Cell.LENGTH; j++) {
                cells.get(i).add(j, new Cell(i * Cell.LENGTH, j * Cell.LENGTH + Cell.LENGTH, Cell.State.DEAD));
            }
        }

        //CREATES LIST OF CELLS STATES
        for (int i = 0; i < Main.WIDTH / Cell.LENGTH; i++) {
            currentCellsStates.add(i, new ArrayList<>());
            for (int j = 0; j < Main.HEIGHT / Cell.LENGTH; j++) {
                currentCellsStates.get(i).add(j, Cell.State.DEAD);
            }
        }
    }

    boolean setCustomBoard() {
        int cellIndexI = X / Cell.LENGTH;
        int cellIndexJ = (Y - Cell.LENGTH) / Cell.LENGTH;

        try {
            cells.get(cellIndexI).get(cellIndexJ).revertState();
        } catch (IndexOutOfBoundsException e) {
            //
        }


        return true;
    }

    boolean isChanged() {
        boolean isChanged = false;

        for (int i = 0; i < Main.WIDTH / Cell.LENGTH; i++) {
            for (int j = 0; j < Main.HEIGHT / Cell.LENGTH; j++) {
                if (currentCellsStates.get(i).get(j) != cells.get(i).get(j).getState()) {
                    isChanged = true;
                    currentCellsStates.get(i).add(j, cells.get(i).get(j).getState());
                }
            }
        }
        return isChanged;
    }

    void nextGeneration() {
        for (int i = 0; i < Main.WIDTH / Cell.LENGTH; i++) {
            for (int j = 0; j < Main.HEIGHT / Cell.LENGTH; j++) {
                cells.get(i).get(j).nextGeneration(getAliveNeighbours(i, j));
            }
        }
    }

    void draw(Graphics graphics) {
        for (int i = 0; i < Main.WIDTH / Cell.LENGTH; i++) {
            for (int j = 0; j < Main.HEIGHT / Cell.LENGTH; j++) {
                cells.get(i).get(j).draw(graphics);
            }
        }
    }

    void drawCustomCreateMode(Graphics graphics) {
        for (int i = 0; i < Main.WIDTH / Cell.LENGTH; i++) {
            for (int j = 0; j < Main.HEIGHT / Cell.LENGTH; j++) {
                cells.get(i).get(j).drawCustomMode(graphics);
            }
        }
    }

    private int getAliveNeighbours(int iIndex, int jIndex) {
        int result = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                try {
                    if (cells.get(iIndex + i).get(jIndex + j).getState() == Cell.State.ALIVE) {
                        result++;
                    }
                } catch (IndexOutOfBoundsException e) {
                    //missed neighbour
                }
            }
        }

        return result;
    }
}
