package ConwaysGameOfLife;

import javax.swing.*;
import java.awt.*;

public class Main extends JFrame {

    static int WIDTH = 800;
    static int HEIGHT = 640;
    static boolean onBoardCustomCreate = true;

    private static Main frame;
    private static int generation = 1;
    private static Board board = new Board();

    private Main() {
        setBounds(0, 0, WIDTH, HEIGHT);
        setBackground(Color.WHITE);
        setFocusable(true);
        setTitle("Conway`s game of life");
        setResizable(false);
        setVisible(true);
    }

    private void run(Graphics graphics) {
        //TODO next
        // save cells configuration after customCreate() and load on start, clearAll(),
        // cell lifetime(in different colors), drag drowning(in listeners) zooming, scrolling and bigger board
        boardCustomCreate(graphics);

        do {
            paint(graphics);
            board.nextGeneration();
            generation++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (board.isChanged());
        paint(graphics);
    }

    void boardCustomCreate(Graphics graphics) {
        Listeners mouseListener = new Listeners();
        frame.addMouseListener(mouseListener);

        board.createBoard();

        while (onBoardCustomCreate) {
            onBoardCustomCreate = board.setCustomBoard();
            paintCustomModeCreator(graphics);
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        frame.removeMouseListener(mouseListener);
    }

    private void paintCustomModeCreator(Graphics graphics) {
        board.drawCustomCreateMode(graphics);
        graphics.setFont(new Font("SOSATB", Font.BOLD, 13));
        graphics.setColor(Color.MAGENTA);
        graphics.drawString("Exit Custom Creator Mode", WIDTH - 200, HEIGHT - 35);
    }

    public void paint(Graphics graphics) {
        //super.paint(graphics);
        board.draw(graphics);
        drawScoreText(graphics);
    }

    public static void game() {
        frame = new Main();
        frame.run(frame.getGraphics());
    }

    private void drawScoreText(Graphics graphics) {
        graphics.setColor(Color.MAGENTA);
        graphics.setFont(new Font("SOSATB", Font.BOLD, 20));
        graphics.drawString("" + generation, 50, 65);
    }
}
