package ConwaysGameOfLife;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Listeners implements MouseListener {
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1 && e.getX() > Main.WIDTH - 200 && e.getY() > Main.HEIGHT - 55
                && e.getX() < Main.WIDTH - 50 && e.getY() < Main.HEIGHT - 35) {
            Main.onBoardCustomCreate = false;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getX() > 0 && e.getX() < Main.WIDTH && e.getY() > 0 && e.getY() < Main.HEIGHT) {
            Board.X = e.getX();
            Board.Y = e.getY();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
