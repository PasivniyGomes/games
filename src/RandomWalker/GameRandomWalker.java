package RandomWalker;

import javax.swing.JFrame;
import java.awt.*;
import java.util.Scanner;

public class GameRandomWalker extends JFrame {
    private RandomWalker randomWalker;
    public static int WIDTH = 800;
    public static int HEIGHT = 640;
    private int stepCounter = 1;

    public GameRandomWalker() {
        setBounds(0, 0, WIDTH, HEIGHT);
        setBackground(Color.WHITE);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void run() {
        while (true) {
            paint(this.getGraphics());
            stepCounter++;

            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void paint(Graphics graphics) {
        randomWalker.draw(graphics);
        drawCounter(graphics);
    }

    private void drawCounter(Graphics graphics) {
        if (stepCounter % 10 == 0) {
            graphics.setColor(Color.GRAY);
            graphics.fillRect(0, 0, WIDTH, 60);

            graphics.setColor(Color.RED);
            graphics.setFont(new Font("SOSATB", Font.PLAIN, 20));
            graphics.drawString("STEP:" + stepCounter, 30, 55);
        }
    }

    public void game(Scanner scanner) {
        //TODO fix exception with listeners
        System.out.println("~What of walker you want to play?\n" +
                "       1(RandomWalker), 2(RandomLineWalker)");
        int choice = scanner.nextInt();
        if (choice == 1) {
            randomWalker = new RandomWalker(400, 320);
        } else if (choice == 2) {
            randomWalker = new RandomLineWalker(400, 320);
        } else {
            System.out.println("Enter 1 or 2!");
            game(scanner);
        }

        this.run();
    }
}
