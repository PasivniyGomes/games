package RandomWalker;

import java.awt.Graphics;
import java.awt.Color;
import java.util.Random;


public class RandomWalker {
    private int x, y;

    public RandomWalker(int x, int y) {
        this.x = x;
        this.y = y;
    }

    void draw(Graphics graphics) {
        walk();
        display(graphics);
    }

    protected void display(Graphics graphics) {
        graphics.setColor(getRandomColor());
        graphics.fillRect(x, y, 3, 3);
    }

    private Color getRandomColor() {
        Random rand = new Random();

        return new Color(
                rand.nextFloat(),
                rand.nextFloat(),
                rand.nextFloat());
    }

    protected void walk() {
        int choice = (int) (Math.random() * 4);
        if (choice == 0) {
            x += 3;
        } else if (choice == 1) {
            x -= 3;
        } else if (choice == 2) {
            y += 3;
        } else {
            y -= 3;
        }

        if (x > GameRandomWalker.WIDTH) {
            x = 0;
        } else if (x < 0) {
            x = GameRandomWalker.WIDTH;
        } else if (y > GameRandomWalker.HEIGHT) {
            y = 60;
        } else if (y < 60) {
            y = GameRandomWalker.HEIGHT;
        }
    }

    public void changeX(int x) {
        this.x += x;
    }

    public void changeY(int y) {
        this.y += y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
