package RandomWalker;

import java.awt.Graphics;
import java.awt.Color;

public class RandomLineWalker extends RandomWalker{
    private enum Direction {
        UP, DOWN, LEFT, RIGHT;
    }

    private Direction direction;

    public RandomLineWalker(int x, int y) {
        super(x, y);
    }

    @Override
    protected void display(Graphics graphics) {
        graphics.setColor(Color.BLACK);

        if (direction == Direction.RIGHT) {
            graphics.fillRect(getX(), getY(), 5, 1);
        } else if (direction == Direction.LEFT) {
            graphics.fillRect(getX(), getY(), 5, 1);
        } else if (direction == Direction.DOWN) {
            graphics.fillRect(getX(), getY(), 1, 5);
        } else if (direction == Direction.UP) {
            graphics.fillRect(getX(), getY(), 1, 5);
        }
    }

    @Override
    protected void walk() {
        int choice = (int) (Math.random() * 4);
        if (choice == 0) {
            changeX(5);
            direction = Direction.RIGHT;
        } else if (choice == 1) {
            changeX(-5);
            direction = Direction.LEFT;
        } else if (choice == 2) {
            changeY(5);
            direction = Direction.DOWN;
        } else {
            changeY(-5);
            direction = Direction.UP;
        }

        if (getX() > GameRandomWalker.WIDTH) {
            setX(0);
        } else if (getX() < 0) {
            setX(GameRandomWalker.WIDTH);
        } else if (getY() > GameRandomWalker.HEIGHT) {
            setY(60);
        } else if (getY() < 60) {
            setY(GameRandomWalker.HEIGHT);
        }
    }
}

