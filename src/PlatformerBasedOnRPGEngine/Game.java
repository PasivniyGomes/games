package PlatformerBasedOnRPGEngine;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class Game extends JFrame implements Runnable {
    //Color to make transparency ~= magenta
    static final int alpha = 0xFFFF00DC;
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 800;
    private int xZoom = 3;
    private int yZoom = 3;

    private Canvas canvas = new Canvas();
    private RenderHandler renderer;

    private KeyBoardListener keyListener = new KeyBoardListener(this);
    private MouseEventListener mouseListener = new MouseEventListener(this);

    private Map map;

    private GameObject[] gameObjects;
    private Player player;

    private int selectedTileID = 2;

    public Game() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, WIDTH, HEIGHT);
        setTitle("Engine Test");

        //Put our frame in the center of the screen.
        setLocationRelativeTo(null);

        add(canvas);
        setVisible(true);

        canvas.createBufferStrategy(3);
        renderer = new RenderHandler(getWidth(), getHeight());

        //LOAD AND INIT MAP, TILES, SHEETS, ETC
        SpriteSheet sheet = new SpriteSheet(Objects.requireNonNull(loadImage("RPG/tiles/Tiles1.png")));
        sheet.loadSprites(16, 16);
        SpriteSheet playerSheet = new SpriteSheet(Objects.requireNonNull(loadImage("RPG/tiles/Player.png")));
        playerSheet.loadSprites(20, 26);

        //Player Animated Sprites
        AnimatedSprite playerAnimations = new AnimatedSprite(playerSheet, 5);

        Tiles tiles = new Tiles(new File("C:\\Users\\User\\Desktop\\Games\\src\\RPG\\Tiles.txt"), sheet);
        map = new Map(new File("C:\\Users\\User\\Desktop\\Games\\src\\RPG\\Map.txt"), tiles);

        //LOAD GUIElements
        Sprite[] tileSprites = tiles.getSprites();
        GUIButton[] buttons = new GUIButton[tiles.size()];
        for (int i = 0; i < buttons.length; i++) {
            Rectangle rect = new Rectangle(i * (16 * xZoom + 2), HEIGHT - 100, 16 * xZoom, 16 * yZoom);
            buttons[i] = new InventoryButton(this, i, tileSprites[i], rect);
        }
        GUI gui = new GUI(buttons, 5, 5, true);

        //INIT GAME OBJECTS
        gameObjects = new GameObject[2];
        player = new Player(playerAnimations);
        gameObjects[0] = player;
        gameObjects[1] = gui;



        //ADD LISTENERS
        canvas.addKeyListener(keyListener);
        canvas.addFocusListener(keyListener);
        canvas.addMouseListener(mouseListener);
        canvas.addMouseMotionListener(mouseListener);
    }

    private BufferedImage loadImage(String path) {
        try {
            BufferedImage loadedImage = ImageIO.read(Objects.requireNonNull(Game.class.getClassLoader().getResource(path)));
            BufferedImage formattedImage = new BufferedImage(loadedImage.getWidth(), loadedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
            formattedImage.getGraphics().drawImage(loadedImage, 0, 0, null);

            return formattedImage;
        } catch (IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    private void update() {
        for (GameObject gameObject : gameObjects) {
            gameObject.update(this);
        }
    }

    private void render() {
        BufferStrategy bufferStrategy = canvas.getBufferStrategy();
        Graphics graphics = bufferStrategy.getDrawGraphics();
        super.paint(graphics);

        map.render(renderer, xZoom, yZoom);
        for (GameObject gameObject : gameObjects) {
            gameObject.render(renderer, xZoom, yZoom);
        }
        renderer.render(graphics);
        renderer.renderPlayerCoordinates(graphics, player, xZoom, yZoom);

        graphics.dispose();
        bufferStrategy.show();

        //CLEAR THE LAST GRAPHICS TO AVOID GLITCHES
        renderer.clear();
    }

    @Override
    public void run() {
        BufferStrategy bufferStrategy = canvas.getBufferStrategy();

        long lastTime = System.nanoTime();
        double nanoSecondConversion = 1000000000.0 / 60; //60 frames per second
        double changeInSeconds = 0;

        while (true) {
            long now = System.nanoTime();

            changeInSeconds += (now - lastTime) / nanoSecondConversion;
            while (changeInSeconds >= 1) {
                update();
                changeInSeconds--;
            }

            render();
            lastTime = now;
        }
    }

    void leftClick(int x, int y) {
        Rectangle mouseRectangle = new Rectangle(x, y, 1, 1);
        boolean stoppedChecking = false;

        for (GameObject object : gameObjects) {
            if (!stoppedChecking) {
                stoppedChecking = object.handleMouseClick(mouseRectangle, renderer.getCamera(), xZoom, yZoom);
            }
        }

        if (!stoppedChecking) {
            x = (int) Math.floor((x + renderer.getCamera().x) / (16.0 * xZoom));
            y = (int) Math.floor((y + renderer.getCamera().y) / (16.0 * yZoom));
            map.setTile(x, y, selectedTileID);
        }
    }

    void rightClick(int x, int y) {
        x = (int) Math.floor((x + renderer.getCamera().x) / (16.0 * xZoom));
        y = (int) Math.floor((y + renderer.getCamera().y) / (16.0 * yZoom));

        map.removeTile(x, y);
    }

    //Pressed Ctrl+S - SAVE THE MAP
    void handleCTRL(boolean[] keys) {
        if (keys[KeyEvent.VK_S]) {
            map.saveMap();
            System.out.println("~Map saved!");
        }
    }

    KeyBoardListener getKeyListener() {
        return keyListener;
    }

    public MouseEventListener getMouseListener() {
        return mouseListener;
    }

    RenderHandler getRenderer() {
        return renderer;
    }

    void changeTile(int tileID) {
        selectedTileID = tileID;
    }

    int getSelectedTile() {
        return selectedTileID;
    }

    public static void main(String[] args) {
        Game game = new Game();
        Thread gameThread = new Thread(game);
        gameThread.start();
    }
}
