package PlatformerBasedOnRPGEngine;

import java.awt.image.BufferedImage;

public class AnimatedSprite extends Sprite implements GameObject {

    private Sprite[] sprites;
    private int currentSprite = 0;
    private int speed;
    private int counter;

    private int startSprite;
    private int endSprite;

    //if speed = 60, it`s animate 1 times per second
    //speed = 1 =>   it`s animate 60 times per second(every frame)
    public AnimatedSprite(BufferedImage[] images, int speed) {
        sprites = new Sprite[images.length];
        startSprite = images.length - 1;

        for (int i = 0; i < images.length; i++) {
            sprites[i] = new Sprite(images[i]);
        }
    }

    public AnimatedSprite(SpriteSheet sheet, Rectangle[] positions, int speed) {
        sprites = new Sprite[positions.length];
        this.speed = speed;
        endSprite = positions.length - 1;

        for (int i = 0; i < positions.length; i++) {
            sprites[i] = new Sprite(sheet, positions[i].x, positions[i].y, positions[i].width, positions[i].height);
        }
    }

    public AnimatedSprite(SpriteSheet sheet, int speed) {
        sprites = sheet.getLoadedSprites();
        this.speed = speed;
        endSprite = sprites.length - 1;
    }

    @Override
    public void render(RenderHandler renderer, int xZoom, int yZoom) {

    }

    @Override
    public void update(Game game) {
        counter++;
        if (counter > speed) {
            counter = 0;
            incrementSprite();
        }
    }

    @Override
    public boolean handleMouseClick(Rectangle mouseRectangle, Rectangle camera, int xZoom, int yZoom) {
        return false;
    }

    void reset() {
        counter = 0;
        currentSprite = startSprite;
    }

    void incrementSprite() {
        currentSprite++;
        if (currentSprite >= endSprite) {
            currentSprite = startSprite;
        }
    }

    void setAnimationRange(int startSprite, int endSprite) {
        this.startSprite = startSprite;
        this.endSprite = endSprite;
        reset();
    }

    @Override
    int getWidth() {
        return sprites[currentSprite].getWidth();
    }

    @Override
    int getHeight() {
        return sprites[currentSprite].getHeight();
    }

    @Override
    int[] getPixels() {
        return sprites[currentSprite].getPixels();
    }
}
