package PlatformerBasedOnRPGEngine;

class GUI implements GameObject {

    private Sprite backgroundSprite;
    private GUIButton[] buttons;
    private Rectangle rect = new Rectangle();
    private boolean fixed;

    GUI(Sprite backgroundSprite, GUIButton[] buttons, int x, int y, boolean fixed) {
        this.fixed = fixed;
        this.backgroundSprite = backgroundSprite;
        this.buttons = buttons;
        rect.x = x;
        rect.y = y;
        if (backgroundSprite != null) {
            rect.width = backgroundSprite.getWidth();
            rect.height = backgroundSprite.getHeight();
        }
    }

    GUI(GUIButton[] buttons, int x, int y, boolean fixed) {
        this(null, buttons, x, y, fixed);
    }

    @Override
    public void render(RenderHandler renderer, int xZoom, int yZoom) {
        if (backgroundSprite != null) {
            renderer.renderSprite(backgroundSprite, rect.x, rect.y, xZoom, yZoom, fixed);
        }
        if (buttons != null) {
            for (int i = 0; i < buttons.length; i++) {
                buttons[i].render(renderer, xZoom, yZoom, rect);
            }
        }
    }

    @Override
    public void update(Game game) {
        if (buttons != null) {
            for (GUIButton button : buttons) {
                button.update(game);
            }
        }
    }

    @Override
    public boolean handleMouseClick(Rectangle mouseRectangle, Rectangle camera, int xZoom, int yZoom) {
        if (!fixed) {
            mouseRectangle = new Rectangle(mouseRectangle.x + camera.x, mouseRectangle.y + camera.y, 1, 1);
        } else {
            mouseRectangle = new Rectangle(mouseRectangle.x, mouseRectangle.y, 1, 1);
        }

        mouseRectangle.x += rect.x;
        mouseRectangle.y += rect.y;
        boolean clicked = false;

        if (mouseRectangle.intersects(rect) || rect.width == 0 || rect.height == 0) {
            for (GUIButton button : buttons) {
                if (!clicked) {
                    clicked = button.handleMouseClick(mouseRectangle, camera, xZoom, yZoom);
                } else return true;
            }
        }
        return clicked;
    }
}
