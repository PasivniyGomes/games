package PlatformerBasedOnRPGEngine;

import java.awt.image.BufferedImage;

class Sprite {
    private int[] pixels;
    private int width, height;

    Sprite(SpriteSheet sheet, int startX, int startY, int width, int height) {
        this.width = width;
        this.height = height;
        pixels = new int[width * height];
        sheet.getImage().getRGB(startX, startY, width, height, pixels, 0, width);
    }

    Sprite(BufferedImage image) {
        width = image.getWidth();
        height = image.getHeight();
        pixels = new int[width * height];
        image.getRGB(0, 0, width, height, pixels, 0, width);
    }

    Sprite() {

    }

    int[] getPixels() {
        return pixels;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }
}
