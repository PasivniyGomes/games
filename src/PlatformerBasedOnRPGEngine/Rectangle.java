package PlatformerBasedOnRPGEngine;

class Rectangle {
    private int[] pixels;
    int x, y, width, height;

    Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Creates empty Rectangles(0, 0, 0, 0)
     */
    Rectangle() {
        this(0, 0, 0, 0);
    }

    void generateGraphics(int color) {
        pixels = new int[width * height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixels[x + y * width] = color;
            }
        }
    }


    boolean intersects(Rectangle otherRectangle) {
        if (x > otherRectangle.x + otherRectangle.width || otherRectangle.x > x + width) {
            return false;
        }

        if (y > otherRectangle.y + otherRectangle.height || otherRectangle.y > y + height) {
            return false;
        }

        return true;
    }

    /**
     * Draw (NOT FILL) frame with borderWidth
     */
    void generateGraphics(int borderWidth, int color) {
        pixels = new int[width * height];

        //Fill all rectangle with alpha color
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = Game.alpha;
        }

        //Upper side
        for (int y = 0; y < borderWidth; y++) {
            for (int x = 0; x < width; x++) {
                pixels[x + y * width] = color;
            }
        }

        //Left side
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < borderWidth; x++) {
                pixels[x + y * width] = color;
            }
        }

        //Right side
        for (int y = 0; y < height; y++) {
            for (int x = width - borderWidth; x < width; x++) {
                pixels[x + y * width] = color;
            }
        }

        //Downside
        for (int y = height - borderWidth; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixels[x + y * width] = color;
            }
        }
    }

    int[] getPixels() {
        if (pixels != null)
            return pixels;
        else
            System.out.println("Returned null instead of pixels[], because of non-initialized pixels[]" +
                    "\n\t\t(Rectangle.getPixels())");

        return null;
    }
}
