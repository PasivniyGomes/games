import ConwaysGameOfLife.Main;
import PointDestructionGame.StartGame;
import RandomWalker.*;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Game {

    static Scanner scanner = new Scanner(System.in);
    private static boolean isRunning = true;
    private static Commands commands = new Commands();
    private static Set<String> games = new HashSet<>();


    public static void main(String[] args) {
        games.add("game of life (life)");
        games.add("random walker (walker)");
        games.add("point destruction game (point)");

        commands.help();
        while(isRunning) {
            askForCommands();
        }
    }

    private static void askForCommands() {
        String choice = scanner.nextLine();

        if (choice.equals("help")) {
            commands.help();
            return;
        } else if (choice.equals("exit")) {
            commands.exit();
            isRunning = false;
            return;
        } else if (choice.equals("list")) {
            commands.list(games);
            return;
        }

        String[] splitedChoice = choice.split(" ");

        if (splitedChoice[0].equals("play")) {
            if (splitedChoice.length == 1) {
                System.out.println("missing parameter <game_name>!");
            } else {
                try {
                    commands.play(concateFromSecondWord(splitedChoice));
                } catch (IllegalArgumentException e) {
                    System.out.println("game with name '" + concateFromSecondWord(splitedChoice) + "' isn`t exist!");
                }
            }
        } else {
            System.out.println("Command '" + splitedChoice[0] + "' isn`t exist! Type 'help' for more details");
        }
    }

    private static String concateFromSecondWord(String[] s) {
        String result = "";
        for (int i = 1; i < s.length; i++) {
            result += s[i] + " ";
        }
        return result;
    }
}

class Commands {
    void help() {
        System.out.println("        ~~HELP PAGE~~\n" +
                "help                   -  get help about commands\n" +
                "list                   -  display all available games\n" +
                "play <game_name>        -  play game with name 'game_name'\n" +
                "exit                   -  exit the application\n");
    }

    void exit() {
        System.out.println("Thank`s for using my programme!\n" +
                "       ~~See you soon.");
    }

    void list(Set<String> games) {
        int counter = 1;

        for (String game : games) {
            System.out.println(counter + ") " + game);
            counter++;
        }
    }

    void play(String game) throws IllegalArgumentException {
        switch (game) {
            case "game of life ": {
                Main.game();
                break;
            }
            case "life ": {
                Main.game();
                break;
            }
            case "random walker ": {
                GameRandomWalker gameRandomWalker = new GameRandomWalker();
                gameRandomWalker.game(Game.scanner);
                break;
            }
            case "walker ": {
                GameRandomWalker gameRandomWalker = new GameRandomWalker();
                gameRandomWalker.game(Game.scanner);
                break;
            }
            case "point destruction game ": {
                StartGame.game();
                break;
            }
            case "point ": {
                StartGame.game();
                break;
            }
            default: throw new IllegalArgumentException();


        }
    }
}
