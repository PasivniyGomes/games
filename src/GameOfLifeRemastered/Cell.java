package GameOfLifeRemastered;

import java.awt.*;

class Cell {
    private int x;
    private int y;
    private State state;
    private State nextState;

    enum State {
        DEAD, ALIVE
    }

    public Cell(int x, int y, State state) {
        this.x = x;
        this.y = y;
        this.state = state;
    }

    void nextGeneration(int aliveNeighbours) {
        if (state == State.ALIVE) {
            if (aliveNeighbours < 2) {
                nextState = State.DEAD;
            } else if (aliveNeighbours <= 3) {
                nextState = State.ALIVE;
            } else {
                nextState = State.DEAD;
            }
        }
        if (state == State.DEAD) {
            if (aliveNeighbours == 3) {
                nextState = State.ALIVE;
            }
        }
    }

    void setNextState() {
        state = nextState;
    }

    void draw(Graphics g, int cellLength) {
        Graphics2D graphics = (Graphics2D) g;
        graphics.setStroke(new BasicStroke(1));

        if (state == State.DEAD) {
            graphics.setColor(Color.WHITE);
            graphics.fillRect(x, y, cellLength, cellLength);
        } else if (state == State.ALIVE) {
            graphics.setColor(Color.BLACK);
            graphics.fillRect(x, y, cellLength, cellLength);
        }
        graphics.setColor(Color.GRAY);
        graphics.drawRect(x, y, cellLength, cellLength);
    }

    void revertState() {
        if (state == State.ALIVE) {
            state = State.DEAD;
        } else {
            state = State.ALIVE;
        }
    }

    State getState() {
        return state;
    }
}
