package GameOfLifeRemastered;

public class Main {

    private GUI gui;
    private Board board;
    static final int CELL_LENGTH = 100;
    static final int WIDTH = 800;
    static final int HEIGHT = 640;
    static boolean isPause;
    private int generation = 1;
    static State state = State.MENUE;
    enum State {
        MENUE, GAME, EXIT
    }

    private Main() {
        isPause = false;
        gui = new GUI();
        board = new Board(CELL_LENGTH);
        board.generateBoardWithDeadCells();
    }

    private void inMenue() {
        gui.drawMenue();
    }

    private void inGame() {
        if (isPause) {
            board.askForCustomPainting();
            board.draw(gui.getGraphics());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            board.nextGeneration();
            board.draw(gui.getGraphics());
            generation++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void exit() {
        //SAY GOODBYE OR SOMETHING ELSE
    }

    private void run() {
        boolean isRunning = true;

        while(isRunning) {
            if (state.equals(State.MENUE)) {
                inMenue();
            } else if (state.equals(State.GAME)) {
                inGame();
            } else if (state.equals(State.EXIT)) {
                isRunning = false;
                exit();
            }
            System.out.println(generation + " GENERATION");
        }
    }

    //On running the game
    public static void main(String[] args) {
        Main game = new Main();
        game.run();
    }
}
