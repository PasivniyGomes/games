package GameOfLifeRemastered;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

class GUI extends JFrame {

    static private JButton playButton;
    private boolean isPlayButtonDisabled;

    GUI() {
        setBounds(0, 0, Main.WIDTH, Main.HEIGHT);
        setTitle("Conway`s Game Of Life Remastered");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setFocusable(true);
        setAlwaysOnTop(true);
        setResizable(false);
        setVisible(true);

        playButton = new JButton("P L A Y");
        playButton.setContentAreaFilled(false);
        int buttonHeight = Main.HEIGHT / 15;
        int buttonWidth = Main.WIDTH / 4;
        Dimension dimension = new Dimension();
        dimension.setSize(buttonWidth, buttonHeight);
        playButton.setPreferredSize(dimension);
        playButton.setMinimumSize(dimension);
        playButton.setMaximumSize(dimension);
        playButton.setBounds(Main.WIDTH / 2 - buttonWidth / 2, Main.HEIGHT / 2 + buttonHeight, buttonWidth, buttonHeight);
        int fontSize = Main.WIDTH / 100 + 10;
        playButton.setFont(new Font("Consolas", Font.BOLD, fontSize));
        isPlayButtonDisabled = true;
        playButton.addActionListener(e -> {
            Main.state = Main.State.GAME;
            isPlayButtonDisabled = true;
            playButton.setEnabled(false);
            playButton.setVisible(false);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        add(playButton);

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SPACE && Main.state.equals(Main.State.GAME)) {
                    if (Main.isPause) {
                        Main.isPause = false;
                    } else {
                        Main.isPause = true;
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (Main.state == Main.State.GAME && Main.isPause) {
                    Board.mouseX = e.getX();
                    Board.mouseY = e.getY();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

    void drawMenue() {
        if (isPlayButtonDisabled) {
            isPlayButtonDisabled = false;
            playButton.setEnabled(true);
            playButton.setVisible(true);
        }
    }
}
