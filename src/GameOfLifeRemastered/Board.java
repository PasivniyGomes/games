package GameOfLifeRemastered;

import java.awt.Graphics;

class Board {


    private Cell[][] board;
    private int cellLength;
    private int row;
    private int column;
    static int mouseX = -1;
    static int mouseY = -1;

    Board(int cellLength) {
        column = Main.WIDTH / cellLength;
        row = Main.HEIGHT / cellLength;
        this.cellLength = cellLength;
        board = new Cell[column][row];
    }

    void nextGeneration() {
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                board[i][j].nextGeneration(getAliveNeighbours(i, j));
            }
        }
        applyNextGeneration();
    }

    private void applyNextGeneration() { //Apply next state for the cells
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                board[i][j].setNextState();
            }
        }
    }

    private int getAliveNeighbours(int iIndex, int jIndex) {
        int result = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                try {
                    if (board[iIndex + i][jIndex + j].getState() == Cell.State.ALIVE) {
                        result++;
                    }
                } catch (IndexOutOfBoundsException e) {
                    //missed neighbour
                }
            }
        }

        return result;
    }

    void draw(Graphics graphics) {
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                board[i][j].draw(graphics, cellLength);
            }
        }
    }

    void askForCustomPainting() {
        int cellIndexI = mouseX / Main.CELL_LENGTH;
        int cellIndexJ = (mouseY - 35) / Main.CELL_LENGTH;

        if (mouseX != -1) { //TO PREVENT BUG WITH AUTOFILLING FIRST TILE
            try {
                board[cellIndexI][cellIndexJ].revertState();
            } catch (IndexOutOfBoundsException e) {
                //
            }
        }
        mouseX = -1;
        mouseY = -1;
    }

    void generateBoardWithDeadCells() {
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                board[i][j] = new Cell(i * Main.CELL_LENGTH, j * Main.CELL_LENGTH + 35, Cell.State.DEAD);
            }
        }
    }
}
